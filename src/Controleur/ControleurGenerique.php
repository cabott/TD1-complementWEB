<?php

namespace TheFeed\Controleur;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use TheFeed\Lib\MessageFlash;
use TheFeed\Lib\Conteneur;

class ControleurGenerique {

    protected static function afficherVue(string $cheminVue, array $parametres = []): Response
    {
        extract($parametres);
        $messagesFlash = MessageFlash::lireTousMessages();
        ob_start();
        require __DIR__ . "/../vue/$cheminVue";
        $corpsReponse = ob_get_clean();
        return new Response($corpsReponse);
    }

    protected static function afficherTwig(string $cheminVue, array $parametres = []): Response
    {
        /** @var Environment $twig */
        $twig = Conteneur::recupererService("twig");
        $corpsReponse = $twig->render($cheminVue, $parametres);
        return new Response($corpsReponse);
    }

    // https://stackoverflow.com/questions/768431/how-do-i-make-a-redirect-in-php
    protected static function rediriger(string $chemin = "", ?array $parametres = []) : RedirectResponse
    {
        $generateurUrl = Conteneur::recupererService("generateurUrl");
        $url = $generateurUrl->generate($chemin,$parametres);
        return new RedirectResponse($url);
    }

    public static function afficherErreur($messageErreur = "", $statusCode = 400): Response
    {
//        $reponse = ControleurGenerique::afficherVue('base.html.twig', [
//            "pagetitle" => "Problème",
//            "cheminVueBody" => "erreur.php",
//            "messageErreur" => $messageErreur
//        ]);
//
//        $reponse->setStatusCode($statusCode);
        return new Response(ControleurGenerique::afficherTwig("erreur.html.twig",["messageErreur" => $messageErreur]));
    }

}